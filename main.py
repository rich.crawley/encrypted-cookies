from flask import Flask, render_template, make_response, request
from cryptography.fernet import Fernet

app = Flask(__name__)
app_key = Fernet.generate_key()
fernet = Fernet(app_key)

@app.route("/")
def home():
    session_id = request.cookies.get("hw_session")

    if session_id is not None:
        session_id = fernet.decrypt(
            session_id.encode()
        ).decode()

    resp = make_response(
        render_template(
            "index.html",
            session_id = session_id
        )
    )

    resp.set_cookie("hw_session", fernet.encrypt(b"12345678").decode())
    return resp

if __name__ == "__main__":
    app.run(debug = True)
